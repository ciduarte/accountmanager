<?php
class Account extends AccountManagerAppModel
{
    public $name = 'Account';
    public $useTable = 'users';

    public $actsAs = array(
        'AccountManager.UsersManagerValidator',
        'AccountManager.UsersManagerRegisterer',
    );

    public $validationDomain = 'account_manager';
    public $authCode = '';

    /**
     *
     * beforeSave.
     *
     * @param array options.
     * @access public.
     * @return void.
     */
     /*
    public function beforeSave($options = array())
    {
        if (! $this->id) {
            $this->authCode = sha1(time());
            $this->data['Account']['active'] = 0;
            $this->data['Account']['auth_code'] = $this->authCode;
        }

        return true;
    }*/
}
?>

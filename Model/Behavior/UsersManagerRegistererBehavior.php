<?php
class UsersManagerRegistererBehavior extends ModelBehavior
{
    public function setup(Model $model, $settings = array())
    {

    }

    /**
     *
     * beforeSave handler
     *
     * @param Model $model
     * @param array options
     *
     * @access public
     * @return bool
     */
    public function beforeSave(Model $model, $options = array())
    {
        if (!$model->id) {
            $model->data[$model->alias]['active'] = 0;
            $model->data[$model->alias]['auth_code'] = sha1(time());
        }

        return true;
    }
}

<?php
class AccountsController extends AccountManagerAppController
{
    public $uses = array();
    public $layout = '';

    public $components = array(
        'AccountManager.Registration' => array(
            'template' => array(
                'view' => "AccountManager.welcome",
                'layout' => ""
            ),
            'accountModel' => "AccountManager.Account",
            'activationUrl' =>  "/account_manager/activate/"
        )
    );

    protected $_config = array(
                             'options' =>
                              array('register' =>
                                    array('url'  => 'register',
                                          'text' => 'REGISTER_OPTION'
                                         ),
                                    'recover'=>
                                    array('url'  => 'recover',
                                          'text' => 'Recover your password'
                                         ),
                                    'login'=>
                                    array('url'  => 'login',
                                          'text' => 'LOGIN_OPTION'
                                         ),
                                    'configure' =>
                                    array('url'  => 'configure',
                                          'text' => 'CONFIGURE_OPTION'
                                         )
                                    ),
                              'mail'=>
                              array('active' => false,
                                    'contact'=> ''
                                   ),
                              'lang' => ''
                             );

    protected $_activeOptions = array();

    /**
     *
     * beforeFilter.
     *
     *
     * Callback that's called before any action in the controller.
     *
     * @param  void.
     * @access public.
     * @return void.
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->ExtendedAuth->loginScope = array('active' =>
                                              array('value' => 1,
                                                    'message' => __d('account_manager', 'NO_ACTIVE_ACCOUNT')),
                                              'blocked' =>
                                              array('value' => 0,
                                                    'message' => __d('account_manager', 'BLOCKED_ACCOUNT')));

        /*Plugin options.*/
        /**********************************************************************/
        /* default */
        $this->_activeOptions['login'] = $this->_config['options']['login'];
        /**********************************************************************/
        foreach ($this->_config['options'] as $keyOption=>$option) {
            if (Configure::read('AccountManager.options.'.$keyOption) == true) {
                $this->_activeOptions[$keyOption] = $option;
            }
        }
        /**********************************************************************/

        $this->layout = Configure::read('AccountManager.layout');
        $this->_config['mail'] = Configure::read('AccountManager.mail');
        $this->_config['lang'] = Configure::read('AccountManager.lang');

        $this->ExtendedAuth->allowedActions = array('login', 'register', 'activate', 'resendActivationCode');
        if (! empty($this->_config['lang'])) {
            Configure::write('Config.language', $this->_config['lang']);
        }
    }

    /**
     *
     * activate.
     *
     * Activates an inactive account using the authentication code passed.
     *
     * @param string $code authentication code generated when the account was created.
     * @access public.
     * @return void.
     */
    public function activate($code = '')
    {
        if (empty($code)) {
            throw new BadRequestException();
        }

        $this->set(array(
            'title_for_layout' => __d('account_manager', 'ACTIVATION_TITLE'),
            'view' => 'register',
            'viewOptions' => $this->_activeOptions)
        );

        if ($this->Registration->activateAccount($code)) {
            $this->render('account_activated');
        } else {
            $this->render('error');
        }
    }

    /**
     *
     * home.
     *
     *
     * @access public.
     * @return void.
     *
     */
    public function home()
    {
        $user  = $this->ExtendedAuth->user();
        $authConfig = current($this->ExtendedAuth->authenticate);

        $descriptionField = $authConfig['fields']['username'];
        $alias = $user[$descriptionField];

        if (! empty($user['alias'])) {
            $alias = $user['alias'];
        }

        $this->set('userAlias', $alias);
    }

    /**
     *
     * register.
     *
     * Register a new account.
     *
     * @param string $mail
     * @access pubic.
     * @return void.
     */
    public function register($mail = '')
    {
        $msg = '';
        $loadGreetinsView = false;

        switch (true) {
            case $this->request->is('post'):
                $this->loadModel('AccountManager.Account');
                $this->Account->set($this->request->data);
                if ($this->Account->save()) {
                    if ($this->_config['mail']['send']) {
                        if ($this->Registration->sendActivationCode($this->request->data['Account']['username'])) {
                            $loadGreetinsView = true;
                        }
                    }
                } else {
                    $msg = current(current($this->Account->validationErrors));
                }

                break;

            default:

                if (! empty($mail)) {
                    $loadGreetinsView = true;
                }

                break;
        }

        $this->set('viewOptions', $this->_activeOptions);

        if (! $loadGreetinsView) {
            $this->set('view', 'register');
            $this->set('title_for_layout', __d('account_manager', 'REGISTER_TITLE'));
            $this->set('errorMsg',$msg);
        } else {
            $this->set('view', 'register');
            $this->render('thanks_for_register');
            $this->set('title_for_layout', __d('account_manager', 'REGISTER_GREETING'));
        }
    }

    /**
     *
     * resendActivationCode.
     *
     * Resends an activation code to an inactive existent account.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function resendActivationCode()
    {
        $this->set('viewOptions', $this->_activeOptions);
        $this->set('view', 'register');
        $this->set('title_for_layout', __d('account_manager', 'SEND_ACTIVATION_CODE_TITLE'));

        switch (true) {
            case $this->request->is('post'):

                if ($this->Registration->sendActivationCode($this->request->data['Account']['username']))
                {
                    $this->layout = 'message';
                    $this->set('title_for_layout', __d('account_manager', 'ACTIVATION_CODE_SENDED'));
                    $this->render('activationCode');
                }
                /*if (empty($this->data)) {
                    $this->set('errorMsg', __d('account_manager', 'ACCOUNT_NOT_FOUND'));
                } elseif ($this->data['Account']['active'] != 1 && ! empty($this->data['Account']['auth_code'])) {
                    $this->sendActivationCode($this->data['Account']['description'],
                                              $this->data['Account']['auth_code']);

                    $this->layout = 'message';
                    $this->set('title_for_layout', __d('account_manager', 'ACTIVATION_CODE_SENDED'));
                    $this->render('activationCode');
                }*/

                break;
        }
    }

    /**
     *
     * login.
     *
     * Authenticate an account.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function login()
    {
        $redirect = false;

        switch (true) {
            case $this->request->is('post'):

                $this->request->data['Account']['description'] = $this->request->data['Account']['username'];
                $this->request->data['Account']['secret_code'] = $this->request->data['Account']['hashPswd'];

                if ($this->ExtendedAuth->login()) {
                    $user = $this->ExtendedAuth->user();

                    if ($this->Account->logAccess($user['id'])) {
                        $redirect = true;
                    }
                } else {
                    unset($this->request->data['Account']['secret_code']);
                    unset($this->request->data['Account']['description']);
                    $this->set('errorMsg', __d('account_manager', $this->ExtendedAuth->authError));
                }

                break;

            default:

                if ($this->ExtendedAuth->user()) {
                    $redirect = true;
                }

                break;
        }

        if ($redirect) {
            $url = $this->ExtendedAuth->redirectUrl();

            if (strpos($url, 'logout') > 0 ) {
                $url = $this->ExtendedAuth->loginRedirect;
            }

            $this->redirect($url);

        }

        $this->set('viewOptions', $this->_activeOptions);
        $this->set('view', 'login');
        $this->set('title_for_layout', __d('account_manager', 'LOGIN_TITLE'));
    }

    /**
     *
     * logout.
     *
     * Logs out an authenticated user.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function logout()
    {
        $user = $this->ExtendedAuth->user();
        if (! empty($user)) {
            if ($this->ExtendedAuth->logout()) {
                $this->loadModel('AccountManager.Account');
                $this->Account->logout($user['id']);
                $this->redirect('login');
            }
        }
    }

    /**
     *
     * configure account settings (users alias).
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function configure()
    {
        $isSaved = false;
        $message = '';

        if ($this->request->is('post')) {
            $this->loadModel('AccountManager.Account');
            $currentUser = $this->ExtendedAuth->user();

            $data['Account']['id']    = $currentUser['id'];
            $data['Account']['alias'] = $this->request->data['Account']['alias'];

            $this->Account->set($data);

            if ($this->Account->validates()) {
                $isSaved = $this->Account->save();
                if ($isSaved) {
                    $message = __d('account_manager', 'CONFIGURE_SUCCESSFULL');
                }
            } else {
                $currentValidationError = current($this->Account->validationErrors);
                $message = $currentValidationError[0];
            }
        }

        $this->set(array('showForm'         => !$isSaved,
                         'title_for_layout' => __d('account_manager', 'CONFIGURE_TITLE'),
                         'viewOptions'      => array('home'=>array('url' => 'home', 'text' => __d('account_manager', 'HOME_OPTION'))),
                         'view'             => 'configure',
                         'accountMessage'   => $message
                  ));
    }

    /**
     *
     * beforeRender.
     *
     * Controller callback method.
     *
     * @param void.
     * @access public.
     * @return void.
     */
    public function beforeRender()
    {
        $this->set('appName', Configure::read('AccountManager.appName'));
    }
}
?>

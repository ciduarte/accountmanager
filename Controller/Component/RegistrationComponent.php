<?php
App::uses("Component", "Controller");
App::uses("CakeEmail", "Network/Email");
class RegistrationComponent extends Component
{
    public $errorMessage = "";

    public $template = array(
        'view' => "",
        'layout' => ""
    );

    public $config = "gmail";
    public $accountModel = "Account";
    public $activationUrl = "/account_manager/activate/";


    public function activateAccount($code)
    {
        $model = ClassRegistry::init($this->accountModel);
        $account = $model->find("first", array(
                'conditions' => array(
                    'auth_code' =>$code
                )
            )
        );

        if ($account) {
            $account[$model->alias]['auth_code'] = '';
            $account[$model->alias]['active'] = 1;

            if ($model->save($account, false)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function sendActivationCode($email, $options = array())
    {
        $model = ClassRegistry::init($this->accountModel);

        $account = $model->find("first", array(
                'conditions' => array(
                    'description' => $email
                )
            )
        );

        if($account) {
            $params = array_merge(array('url' => Router::url($this->activationUrl, true) . $account[$model->alias]['auth_code']), $options);
            return $this->sendMail($account[$model->alias]['description'], __d('account_manager', 'EMAIL_WELCOME_SUBJECT'), $params);
        } else {
            return false;
        }
    }

    public function sendMail($mail, $subject, $params)
    {
        $eMail = new CakeEmail($this->config);

        $eMail->template($this->template['view'], $this->template['layout']);
        $eMail->from('test@gmail.com');
        $eMail->to($mail);
        $eMail->subject($subject);
        $eMail->emailFormat('html');
        $eMail->viewVars($params);

        return $eMail->send();
    }
}

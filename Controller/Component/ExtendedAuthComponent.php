<?php
App::uses('AuthComponent', 'Controller/Component');

class ExtendedAuthComponent extends AuthComponent
{
    public $loginScope = array();

    /**
     *
     * setDefaultConfiguration.
     *
     * Set default values to component's members.
     *
     * @access public.
     * @return void.
     */
    public function setDefaultConfiguration()
    {
        $this->authenticate = array('AccountManager.ExtendedForm' =>
                                    array('userModel' => 'AccountManager.Account',
                                          'fields' => array('username' => 'description',
                                                          'password' => 'secret_code'
                                               )
                                          ),
                                   );

        $this->loginAction   = '/account_manager/accounts/login';
        $this->loginRedirect = Configure::read('AccountManager.loginRedirect');
        $this->autoRedirect  = false;
        $this->authError     = __d('account_manager', 'AUTH_ERROR');
    }

    /**
     * Log a user in.
     *
     * If a $user is provided that data will be stored as the logged in user. If `$user` is empty or not
     * specified, the request will be used to identify a user. If the identification was successful,
     * the user record is written to the session key specified in AuthComponent::$sessionKey. Logging in
     * will also change the session id in order to help mitigate session replays.
     *
     * @param array $user Either an array of user data, or null to identify a user using the current request.
     * @return boolean True on login success, false on failure
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/authentication.html#identifying-users-and-logging-them-in
     */
    public function login($user = null) {
        $this->_setDefaults();

        if (empty($user)) {
            $user = $this->identify($this->request, $this->response);
        }

        if ($user) {
            if($this->_inScope($user)) {
                    $this->Session->renew();
                    $this->Session->write(self::$sessionKey, $user);
            }
        }

        return $this->loggedIn();
    }

    /**
     *
     * _isScope.
     *
     * @param array/mixed $user.
     * @access protected.
     * @return bool.
     */
    protected function _inScope($user)
    {
        foreach ($this->loginScope as $field=>$optionDef)
            if ($user[$field]!=$optionDef['value']) {
                $this->authError = $optionDef['message'];
                return false;
            }

        return true;
    }

    /**
     *
     *  updateUser.
     *
     * @param array/mixed $user.
     * @access public.
     * @return bool.
     */
    public static function updateUser($user)
    {
        if (CakeSession::check(self::$sessionKey)) {
           return CakeSession::write(self::$sessionKey,$user);
        }

        return false;
    }
}
?>

Asore AccountManager
=======
Asore AccountManager is a development effort for creating a CakePHP plugin that offers an account manager functionality.

Installation
----------------
* Place plugin folder in your base app/Plugin folder.
* Load the plugin in your app/Config/bootstrap.php
    CakePlugin::load('AccountManager',array('bootstrap'=>true,'routes'=>true));
* Run in your database the following script Config/Schema/account_manager.sql
	
Some Handy Links
----------------
[CakePHP](http://www.cakephp.org) - The rapid development PHP framework

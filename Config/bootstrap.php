<?php
Configure::write('AccountManager.layout', 'main');
Configure::write('AccountManager.lang', 'eng');

Configure::write('AccountManager.appName',' Account Manager');

Configure::write('AccountManager.loginRedirect', '/pannel/users/admin_profile');
Configure::write('AccountManager.emptyPassword', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');

Configure::write('AccountManager.mail.send', false);
Configure::write('AccountManager.mail.account', 'no-reply@dommy.com');

Configure::write('AccountManager.options.register', false);
Configure::write('AccountManager.options.recover', false);


?>

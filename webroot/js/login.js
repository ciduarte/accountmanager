(function($){
    $(document).ready(function(){

        $('#username').keyEventCannon({'enter':'#password:focus'});
        $('#password').keyEventCannon({'enter':'#send:click'});
        $('#send').on('click',function(event){

            var $form     = $(event.currentTarget).parents('form');
            var $pswd     = $form.children().find('#password');
            var pswdHash  = SHA1($pswd.val());
                
            $pswd.val('');
            $('#hashPswd').val(pswdHash);
            $form.submit();
        })
    });
})(jQuery);

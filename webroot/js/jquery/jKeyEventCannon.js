(function ($) {
    $.fn.keyEventCannon = function (options) {
        var keyLabels = {
            'enter': 13,
            'tag' : 9
        };

        function onKeyDown(event) {
            var keyPressed, keyExpected;

            keyPressed = event.which || event.keyCode;
            for (var key in options) {
                eventTarget = options[key].split(':');
                keyExpected = keyLabels[key] || key;

                if (parseInt(keyExpected, 10) === keyPressed) {
                    event.preventDefault();
                    $(eventTarget[0]).trigger(eventTarget[1]);
                }
            }
        };

        return this.each(function () {
            $(this).on('keydown', onKeyDown);
        }); //chainability.
    };
})(jQuery);
<?php
echo $this->Form->create('Account');
?>
    <h2><?php echo $title_for_layout;?></h2>
    <fieldset>
        <?php
        echo $this->Form->text('username', array('placeholder' => __d('account_manager', 'USERNAME'))), 
             $this->Form->password('password', array('id' => 'password', 'placeholder' => __d('account_manager', 'PASSWORD'))), 
             $this->Form->password('confirm', array('id' => 'confirm', 'placeholder' => __d('account_manager', 'CONFIRM'))), 
             $this->Form->hidden('password_hash', array('id' => 'pswd-hash')), 
             $this->Form->hidden('confirm_hash',array('id'=>'confirm-hash')), 
             $this->Form->button(__d('account_manager', 'REGISTER_SUBMIT'), array('type' => 'button', 'id' => 'send', 'class' => 'btn btn-large btn-primary'));
        ?>
    </fieldset>
<?php
echo $this->Form->end();
?>
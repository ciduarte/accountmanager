<?php
echo $this->Form->create('Account');
?>
    <h2><?php echo $title_for_layout;?></h2>
    <fieldset>
        <?php
        echo $this->Form->text('username',
                               array('id' => 'username', 
                                     'placeholder' => __d('account_manager','USERNAME')
                                    )
                              ), 
             $this->Form->password('password',
                                   array('id' => 'password',
                                         'placeholder' => __d('account_manager','PASSWORD')
                                        )
                                  ), 
             $this->Form->button(__d('account_manager', 'LOGIN_SUBMIT'),
                                 array('id' => 'send',
                                       'type' => 'button',
                                       'class' => 'btn btn-large btn-primary'
                                      )
                                );
        ?>
        <input type="hidden" name="data[Account][hashPswd]" id="hashPswd" />
    </fieldset>
<?php
echo $this->Form->end();
?>
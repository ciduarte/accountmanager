<ul class="wrapper">
    <?php
    foreach($viewOptions as $keyOption=>$optionDef):
        if($keyOption!=$current)
        {    
    ?>
    <li><?php echo $this->Html->link(__d('account_manager', $optionDef['text']), $optionDef['url'])?></li>
    <?php
        }
    endforeach;
    ?>
</ul>
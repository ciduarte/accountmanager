<h2><?php echo $title_for_layout;?></h2>
<?php
if($showForm)
{
    echo $this->Form->create('Account'), 
         $this->Form->text('alias',array('placeholder' => __d('account_manager', 'USER_ALIAS'))), 
         $this->Form->button(__d('account_manager', 'CONFIGURE_SUBMIT'), array('type' => 'submit', 'id' => 'send', 'class' => 'btn btn-large btn-primary')), 
         $this->Form->end();
}
?>
<p>
<?php echo $accountMessage;?>
</p>
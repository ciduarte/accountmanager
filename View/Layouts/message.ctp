<!-- DOCTYPE FOR HTML 5. -->
<!DOCTYPE html>
<html>
    <head>
        <!-- 
            Character Encoding.
            * Server Config (Transpor layer | Headers).
            * UBOM. (At the begining of the File):
            * <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  | Still allowed.
        -->
        <meta charset="UTF-8">
        <title><?php echo $title_for_layout;?></title>
        <?php echo $this->Html->css('/account_manager/css/style.css');?>
        
    </head>
    <body>
        <h1><?php echo $title_for_layout;?></h1>
        <?php echo $content_for_layout;
        echo $this->element('options',array('current' => $view, 'options' => $viewOptions));
        echo $this->Html->script('/account_manager/js/jquery/jquery-1.9.1.js');
        ?>
        <script type="text/javascript">
            jQuery.noConflict();
        </script>
        <?php
        echo $scripts_for_layout;
        ?>
    </body>
</html>

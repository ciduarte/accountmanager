<!-- DOCTYPE FOR HTML 5. -->
<!DOCTYPE html>
<html>
    <head>
        <!--
            Character Encoding.
            * Server Config (Transpor layer | Headers).
            * UBOM. (At the begining of the File):
            * <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  | Still allowed.
        -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title>HTML 5 Template</title>
        <?php echo $this->Html->css('/account_manager/js/bootstrap/css/bootstrap.min.css');?>
        <?php echo $this->Html->css('/account_manager/css/style.css');?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="masthead">
                <h3 class="muted"><?php echo $appName;?></h3>
            </div>
            <div class="wrapper">
                <?php echo $content_for_layout;?>
            </div>
        </div>
        <footer>
            <div class="container">
                <?php
                if(isset($viewOptions))
                    echo $this->element('options', array('current' => $view));
                ?>
            </div>
        </footer>
        <?php
        if(isset($debug) && $debug)
            echo $this->element('sql_dump');

        echo $this->Html->script('/account_manager/js/jquery/jquery-1.9.1.js'),
             $this->Html->script('/account_manager/js/bootstrap/js/bootstrap.min.js'),
             $this->Html->script('/account_manager/js/jquery/jKeyEventCannon.js');
        ?>
        <script type="text/javascript">
            jQuery.noConflict();
        </script>
        <?php
        echo $scripts_for_layout;
        ?>
    </body>
</html>
